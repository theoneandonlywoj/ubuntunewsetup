asdf plugin-add erlang
asdf plugin-add elixir

export KERL_CONFIGURE_OPTIONS="--disable-debug --without-javac"

asdf install erlang 22.1.6
asdf install elixir 1.9.4-otp-22

asdf global erlang 22.1.6
asdf global elixir 1.9.4-otp-22
